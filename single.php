<?php
global $post, $page, $pages, $multipage;
setup_postdata( $post );

$context = Timber::get_context();
$post = Timber::query_post('ThemePost');
$context['post'] = $post;
if ( $multipage ) {
  $context['post']->post_content = $pages[ $page - 1 ];
}

/**
 * Spis treści posta przy stronicowaniu
 */
if (sizeof($pages) > 1) {
  $i = 0;
  $contents_titles = [];
  foreach ($pages as $page_conent) {
    preg_match("#<!-- wp:acf/post-contents {([\s\S]*)} /-->#",$page_conent,$matches);
    $page_number = $i+1;
    if ($matches) {
      $contents_title = json_decode('{'.$matches[1].'}')->data->title;
    } else {
      $contents_title = "Część ".$page_number;
    }
    $contents_titles[$page_number] = $contents_title;
    $i++;
  }
  $i = 0;
  $contents_links = [];
  foreach ($contents_titles as $contents_page_numer => $contents_title) {
    if ($contents_page_numer == 1) {
      $page_nr = '';
    } else {
      $page_nr = $contents_page_numer;
    }
    $page_number = $i+1;
    $contents_links[$page_number] = '<a href="'.get_permalink().$page_nr.'">'.$page_number.'. '.$contents_title.'</a>';
    $i++;
  }
  $context['contents_links'] = $contents_links;
  if (isset($contents_links[$page+1])) {
    $next_page_number = $page+1;
    $next_page_title = $contents_titles[$page+1];
    $context['contents_link_next'] = [
      'link' => get_permalink().$next_page_number,
      'title' => $next_page_title,
      'page_number' => $next_page_number
    ];
  }
  if ($page > 1) {
    $context['contents_link_back'] = 'open';
  }
  $context['pages'] = true;
}

// if (isset($_COOKIE["read_posts"])) {
//   $read_posts_id = json_decode($_COOKIE['read_posts'], true);
//   $read_posts_id_uniq = array_unique($read_posts_id);
// } else {
//   $read_posts_id = [];
// }
// $read_posts_id[] = get_the_ID();
// var_dump($read_posts_id, $read_posts_id_uniq);
// setcookie("read_posts", json_encode($read_posts_id), time()+30*24*60*60);

//setcookie("read_posts", get_the_ID(), time()+30*24*60*60);

/**
 * Sidebar posts from category and number of posts in sidebar
 */
$post_categorys = get_the_category($post->ID);
$post_categorys_array = [];
foreach ($post_categorys as $post_category ) {
  $post_categorys_array[] = $post_category->term_id;
}

$sidebar_post_number = [];
foreach ($post_categorys_array as $sidebar_number) {
  $sidebar_post_number[] = get_field('sidebar_post_nr', 'category_'.$sidebar_number);
}
foreach ($sidebar_post_number as $number) {
  if ($number != NULL) {
    $post_per_page = $number;
  } 
}
if (!isset($post_per_page)) {
  $post_per_page = 2;
}
$args_nie_przegap = array(
  'date_query' => array( 'before' => get_the_date( 'Y-m-d H:i:s', $post->ID ) ),
  'posts_per_page' => $post_per_page,
  'category__in' => $post_categorys_array
);
$context['sidebar_posts'] = Timber::query_posts($args_nie_przegap, 'ThemePost');

$context['coments_form'] = TimberHelper::ob_function('comments_template');

Timber::render( 'views/templates/single.twig', $context );
