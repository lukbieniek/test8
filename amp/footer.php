<footer class="footer">
  <div class="row large-collapse medium-uncollapse small-uncollapse">
    <div class="footer__menus footer__menus--acordion large-12 small-12 medium-12 column">
      <div class="footer__menu footer__menu--copy footer__menu_1">
        <div class="footer__logo">
          <a class="logo-dark" href="<?php bloginfo( 'template_url' ); ?>">
						<amp-img alt="logo"
							src="<?php bloginfo( 'template_url' ); ?>/dist/images/logo.svg"
							width="152"
							height="30"
							layout="fixed">
						</amp-img>
					</a>
          <a class="logo-white" href="<?php bloginfo( 'template_url' ); ?>">
						<amp-img alt="logo"
							src="<?php bloginfo( 'template_url' ); ?>/dist/images/logo-white.svg"
							width="152"
							height="30"
							layout="fixed">
						</amp-img>
					</a>
        </div>
        <div class="footer__copy">&copy; Wszelkie prawa zastrzeżone - <?php echo date('Y'); ?></div>
      </div>

      <div class="footer__menu footer__menu_5">
        <div>
          <?php if (get_field('twiter_link', 'options') || get_field('facebook_link', 'options')) { ?>
            <div class="footer__menu__title">Dołącz do nas</div>
            <div class="footer__social">
							
              <?php if (get_field('twiter_link', 'options')) { ?>
                <a href="<?php echo get_field('twiter_link', 'options'); ?>" class="logo-dark">
									<amp-img alt="twit"
										src="<?php bloginfo( 'template_url' ); ?>/dist/images/twitter.svg"
										width="22"
										height="18"
										layout="fixed">
									</amp-img>
								</a>
                <a href="<?php echo get_field('twiter_link', 'options'); ?>" class="logo-white">
									<amp-img alt="twit"
										src="<?php bloginfo( 'template_url' ); ?>/dist/images/twitter.svg"
										width="22"
										height="18"
										layout="fixed">
									</amp-img>
								</a>
							<?php } ?>

              <?php if (get_field('facebook_link', 'options')) { ?>
                <a href="<?php echo get_field('facebook_link', 'options'); ?>" class="logo-dark">
									<amp-img alt="fb"
										src="<?php bloginfo( 'template_url' ); ?>/dist/images/facebook-f.svg"
										width="11"
										height="20"
										layout="fixed">
									</amp-img>
								</a>
                <a href="<?php echo get_field('facebook_link', 'options'); ?>" class="logo-white">
									<amp-img alt="fb"
										src="<?php bloginfo( 'template_url' ); ?>/dist/images/facebook-f-dark.svg"
										width="11"
										height="20"
										layout="fixed">
									</amp-img>
								</a>
							<?php } ?>

            </div>
					<?php } ?>

        </div>
        <div class="footer__partner footer__partner--mobile">
          <div class="footer__menu__title">Wykonanie</div>
          <a href="https://spiders.agency/">Spiders.agency</a>
        </div>
      </div>

    </div>
  </div>
</footer>