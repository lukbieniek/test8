<?php
/**
 * Search results page
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context          = Timber::context();
$context['title'] = 'Search results for ' . get_search_query();
$context['posts'] = new Timber\PostQuery('ThemePost');
$context['wp_pagenavi'] = sw_wp_pagenavi($wp_query->query_vars['paged'], get_search_link() );
$context['search_fraze'] = get_search_query();
Timber::render( 'views/templates/search.twig', $context );