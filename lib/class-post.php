<?php
/**
 * Please don't change name of this Class bcs
 * it's use in theme and nroom_modules/ files.
 * 
 * Use nrm_ prefix in function name if you want and 
 * this function to endpoint WP-REST_API used by nroom_modules/twig-to-react
 * 
 */
class ThemePost extends TimberPost {
  
  /**
   * Editor must be able to change post title on home.
   * 
   * @since 0.5.0
   * 
   * @param none
   * @return str post title and post title alternative on home
   */
  public function nrm_title() {
    if (is_front_page() && get_field('tytul_alternatywny', $this->ID)) {
      $title = get_field('tytul_alternatywny', $this->ID);
    } else {
      $title = get_the_title($this->ID);
    }
    return $title;
  }

  public function nrm_date() {
    // https://developer.wordpress.org/reference/functions/get_the_date/
    return get_the_date('d F Y', $this->ID);
  }

  /**
   * Retrieve post thubnail
   * 
   * @param none
   * @return array [oryginal, lqip]
   */
  public function nrm_image_src() {
    $swimage = get_alternative_post_thubnail($this->ID);
    
    return $swimage = [
      'oryginal' => $swimage,
      'lqip' => get_lgip_src($swimage)
    ];
  }

  /**
   * Retrieve post author data
   * 
   * @since 0.5.0
   * 
   * @param none
   * @return array [name, link, avatar] post author
   */
  public function nrm_author() {
    if (function_exists('get_alternative_avatar')) {
      if (get_alternative_avatar($this->post_author)) {
        $author_avatar = get_alternative_avatar($this->post_author);
      } else {
        $author_avatar = get_avatar_url($this->post_author);
      }
    } else {
      $author_avatar = get_avatar_url($this->post_author);
    }

    return $author = [
      'name' => get_the_author_meta('display_name', $this->author->ID),
      'link' => get_author_posts_url($this->author->ID),
      'avatar' => $author_avatar
    ];
  }

  public function nrm_top_level_categories() {
    $categories = get_categories( array(
      'orderby' => 'name',
      'parent'  => 0,
      'include' => wp_get_post_categories($this->ID)
    ) );
    $i=0;
    foreach ( $categories as $category ) {
      return '<a href="'.get_category_link( $category->term_id ).'">'.$category->name.'</a>';
      if($i==0) break;
    }
  }

  public function nrm_product_placement() {
    if (get_field('product_placement', $this->ID)) {
      return '<div class="label">Lokowanie produktu</div>';
    }
  }

  public function nrm_hot_title() {
    if (get_interaction_number( $this->ID ) >= get_field('hot_topic_interaction', 'option')) {
      return '<div class="label label--hot">Gorący temat</div>';
    } 
  }

  public function nrm_comments_btn() {
    $comments_number = get_comments_number( $this->ID );
    if ($comments_number == 0) {
      return '<a href="#post__footer" class="label label--comment">DOŁĄCZ DO DYSKUSJI</a>';
    } else {
      return '<a href="#post__footer" class="label label--comment">KOMENTARZY: '.$comments_number.'</a>';
    }
  }

  public function nrm_excerpt() {
    if (get_field('kia_subtitle', $this->ID)) {
      $excerpt = get_field('kia_subtitle', $this->ID);
    } else {
      if(has_excerpt($this->ID)) {
        $excerpt = get_the_excerpt($this->ID);
      } else {
        $excerpt = '';
      }
    }
    return '<div class="post-boks__excerpt">'.$excerpt.'</div>';
  }

  public function nrm_extra() {
    if (is_single($this->ID) && get_field('ekstra_post', $this->ID) ) {
      return 'post--extra';
    } else {
      return false;
    }
  }

  public function nrm_extra_thubnail() {
    if (is_single($this->ID) && get_field('ekstra_post', $this->ID) ) {
      return 'style="background-image: url('.get_the_post_thumbnail_url($this->ID).');"';
    } else {
      return '';
    }
  }

  public function nrm_brend() {
    if (get_field('post_brend', $this->ID)) {
      return '<img src="'.get_field('partner_logo', $this->ID).'" alt="partner logo" />';
    }
  }

  public function nrm_categorys() {
    $post_categorys = wp_get_post_categories($this->ID);
    $catgory_names = [];
    foreach ($post_categorys as $category_id) {
      $catgory_name = get_cat_name($category_id);
      $catgory_names[] = $catgory_name;
    }
    return implode(" ", $catgory_names);
  }

  public function nrm_share_links() {
    if (function_exists('get_fb_share_link') && function_exists('get_twiter_share_link')) {
      $url = get_permalink( $this->ID );
      $text = $this->post_title;
      $links = [
        'facebook'  => get_fb_share_link( $url ),
        'twiter'    => get_twiter_share_link($url, $text),
        'mesenger'  => get_mesenger_link($url)
      ];

      return $links;
    } else {
      return '<span style="color:red;">error: no sw_module installed: sw-share (https://bitbucket.org/spidersagency/sw-share/)</span>';
    }
  }
}