<?php
/**
 * Conditional tags for Timber
 */

function add_to_context( $data ){
   $data['is_home'] = is_home();
   $data['is_front_page'] = is_front_page();
   $data['is_single'] = is_single();
   $data['is_page'] = is_page();
   $data['is_page_template'] = is_page_template();
   $data['is_category'] = is_category();
   $data['is_tag'] = is_tag();
   $data['is_tax'] = is_tax();
   $data['is_author'] = is_author();
   $data['is_date'] = is_date();
   $data['is_year'] = is_year();
   $data['is_month'] = is_month();
   $data['is_day'] = is_day();
   $data['is_archive'] = is_archive();
   $data['is_404'] = is_404();
   $data['is_paged'] = is_paged();
   $data['is_singular'] = is_singular();
   $data['is_main_query'] = is_main_query();
   $data['wp_link_pages'] = wp_link_pages(array('echo' => 0));
   $data['display_pagination'] = is_archive() || is_home();
   $data['menu_header'] = new \Timber\Menu( 'header-menu', ['depth' => 2] );
   $data['like_a_home'] = get_field('home');
   for($number_of_footer_menu = 1; $number_of_footer_menu <= 5; ++$number_of_footer_menu) {
    if( has_nav_menu( 'footer-'.$number_of_footer_menu ) ) {
      $data['footer_'.$number_of_footer_menu] = new \Timber\Menu( 'footer-'.$number_of_footer_menu );
    }
   }
   $data['search_form'] = get_search_form( false );
   $data['dark_mode'] = get_field('dark_mode_mobile', 'options');
   $data['facebook'] = get_field('facebook_link', 'options');
   $data['twiter'] = get_field('twiter_link', 'options');
   $data['comments_open'] = comments_open();
   return $data;
}
add_filter( 'timber_context', 'add_to_context' );

/**
 * Config vars
 */

function add_to_config( $data ) {
  /**
   * Example
   * $data['config']['name'] = 'value'l
   */
  return $data;
}

add_filter( 'timber_context', 'add_to_config' );
