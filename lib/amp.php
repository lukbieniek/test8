<?php
add_filter( 'amp_post_template_data', function( $data ) {
	$data['amp_component_scripts'] = array_merge(
		$data['amp_component_scripts'],
		array(
      'amp-iframe' => 'https://cdn.ampproject.org/v0/amp-iframe-latest.js',
      'amp-ad' => 'https://cdn.ampproject.org/v0/amp-ad-latest.js',
      'amp-sticky-ad' => 'https://cdn.ampproject.org/v0/amp-sticky-ad-1.0.js'
		)
	);
	return $data;
} );