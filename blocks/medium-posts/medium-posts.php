<?php
add_action('acf/init', 'm_post_and_sidebar');
function m_post_and_sidebar() {     
  if( function_exists('acf_register_block') ) {
    acf_register_block(array(
      'name' => 'm_post_and_sidebar',
      'title' => __('Medium post z sidebar'),
      'description' => __('A custom testimonial block.'),
      'render_callback' => 'm_post_and_sidebar_block_callback',
      'category'=> 'formatting',
      'icon'=> 'admin-comments',
      'supports' => array(
        'align' => false,
      ),
      //'keywords' => array( 'testimonial', 'quote' ),
    ));
  }
}

function m_post_and_sidebar_block_callback( $block ) {
  $vars = [];
  switch(get_field("layout")) {
    case "tribus":
      $post_per_page = 3;
      $location = "/blocks/medium-posts/tribus.twig";
      break;
    case "duo":
      $post_per_page = 2;
      $location = "/blocks/medium-posts/duo.twig";
      break;
    case "duo-big":
      $post_per_page = 2;
      $location = "/blocks/medium-posts/duo-big.twig";
      break;    
  }
  create_block($block, $post_per_page, $vars, $location, false);
}
