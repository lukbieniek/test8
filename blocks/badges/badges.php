<?php
add_action('acf/init', 'badges');
function badges() {     
  if( function_exists('acf_register_block') ) {
    acf_register_block(array(
      'name' => 'badges',
      'title' => __('Badges'),
      'description' => __('A custom testimonial block.'),
      'render_callback' => 'badges_block_callback',
      'category'=> 'formatting',
      'icon'=> 'admin-comments',
      'supports' => array(
        'align' => false,
      ),
      //'keywords' => array( 'testimonial', 'quote' ),
    ));
  }
}

function badges_block_callback( $block ) {
  $context = Timber::context();
  $context['block'] = $block;
  $context['fields'] = get_fields();

  $context['badge_type_name'] = get_field('badge_type');
  $context['badge_type_icon'] = 'badge-'.preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', transliterator_transliterate('Any-Latin; Latin-ASCII', $context['badge_type_name'])));

  Timber::render( 'blocks/badges/badges.twig', $context );
}
