<?php
add_action('acf/init', 'test_platform');
function test_platform() {     
  if( function_exists('acf_register_block') ) {
    acf_register_block(array(
      'name' => 'test_platform',
      'title' => __('Platforma testowa'),
      'description' => __('A custom testimonial block.'),
      'render_callback' => 'test_platform_block_callback',
      'category'=> 'formatting',
      'icon'=> 'admin-comments',
      'supports' => array(
        'align' => false,
      ),
      //'keywords' => array( 'testimonial', 'quote' ),
    ));
  }
}

function test_platform_block_callback( $block ) {
  $context = Timber::context();
  $context['block'] = $block;
  $context['fields'] = get_fields();
  $keys = array_keys(get_field('presety')[0]);
  $result = preg_grep('/row_\d/', $keys);
  $rows = [];
  foreach ( get_field('presety')[0] as $key => $value ) {
    foreach ($result as $row ) {
      if ($key == $row ) {
        $rows[] = $value;
      }
    }
  }
  $context['rows'] = $rows;
  Timber::render( 'blocks/test-platform/test-platform.twig', $context );
}
