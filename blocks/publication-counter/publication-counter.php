<?php
add_action('acf/init', 'publication_counter');
function publication_counter() {     
  if( function_exists('acf_register_block') ) {
    acf_register_block(array(
      'name' => 'publication_counter',
      'title' => __('Publication Counter'),
      'description' => __('A custom testimonial block.'),
      'render_callback' => 'publication_counter_block_callback',
      'category'=> 'formatting',
      'icon'=> 'admin-comments',
      'supports' => array(
        'align' => false,
      ),
      //'keywords' => array( 'testimonial', 'quote' ),
    ));
  }
}

function publication_counter_block_callback( $block ) {
  $context = Timber::context();
  $context['block'] = $block;
  $context['fields'] = get_fields();
  $tax_id = get_queried_object()->term_id;
  $today = getdate();
  if (is_category()) {
    $posts= get_posts(array(
      'numberposts' => -1,
      'post_status' => 'publish',
      'orderby' => 'date',
      'order'   => 'DESC',
      'cat' => $tax_id,
      'date_query' => array(
        array(
          'year'  => $today['year'],
          'month' => $today['mon'],
          'day'   => $today['mday'],
        ),
      ),
    ));
  } elseif (is_tag()) {
    $posts= get_posts(array(
      'numberposts' => -1,
      'post_status' => 'publish',
      'orderby' => 'date',
      'order'   => 'DESC',
      'tag_id' => $tax_id,'date_query' => array(
        array(
          'year'  => $today['year'],
          'month' => $today['mon'],
          'day'   => $today['mday'],
        ),
      )
    ));
  } elseif (is_front_page() or is_page()) {
    $posts= get_posts(array(
      'numberposts' => -1,
      'post_status' => 'publish',
      'orderby' => 'date',
      'order'   => 'DESC',
      'date_query' => array(
        array(
          'year'  => $today['year'],
          'month' => $today['mon'],
          'day'   => $today['mday'],
        ),
      )
    ));
  } elseif (is_author()) {
    $author = get_user_by( 'slug', get_query_var( 'author_name' ) );
    $posts= get_posts(array(
      'numberposts' => -1,
      'post_status' => 'publish',
      'orderby' => 'date',
      'order'   => 'DESC',
      'author' => $author->ID,
      'date_query' => array(
        array(
          'year'  => $today['year'],
          'month' => $today['mon'],
          'day'   => $today['mday'],
        ),
      )
    ));
  }
  $sum_of_post = count($posts);
  
  $context['post_count'] = $sum_of_post;
  Timber::render( 'blocks/publication-counter/publication-counter.twig', $context );
}
