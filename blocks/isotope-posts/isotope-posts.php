<?php
add_action('acf/init', 'isotpe_posts');
function isotpe_posts() {     
  if( function_exists('acf_register_block') ) {
    acf_register_block(array(
      'name' => 'isotpe_posts',
      'title' => __('Isotope Posts'),
      'description' => __('A custom testimonial block.'),
      'render_callback' => 'isotpe_posts_block_callback',
      'category'=> 'formatting',
      'icon'=> 'admin-comments',
      'supports' => array(
        'align' => false,
      ),
      //'keywords' => array( 'testimonial', 'quote' ),
    ));
  }
}

function isotpe_posts_block_callback( $block ) {
  $context = Timber::context();
  $context = [];
  $context['fields'] = get_fields();
  create_block($block, get_field('post_per_page'), $context, __DIR__);
}
