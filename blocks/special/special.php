<?php
add_action('acf/init', 'special_block');
function special_block() {     
  if( function_exists('acf_register_block') ) {
    acf_register_block(array(
      'name' => 'special',
      'title' => __('Sekcja specjalna'),
      'description' => __('A custom testimonial block.'),
      'render_callback' => 'special_block_callback',
      'category'=> 'formatting',
      'icon'=> 'admin-comments',
      'supports' => array(
        'align' => false,
      ),
      //'keywords' => array( 'testimonial', 'quote' ),
    ));
  }
}

function special_block_callback( $block ) {
  $vars = [];
  $post_per_page = get_field("number_of_posts");
  $context['fields'] = get_fields();

  create_block($block, $post_per_page, $vars, __DIR__);
}
