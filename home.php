<?php
$context = Timber::get_context();
$archive_template = get_post(get_gutneberg_archive_template_id())->post_content;
$context['post']['content'] = render_blocks($archive_template);

$context['wp_pagenavi'] = sw_wp_pagenavi($wp_query->query_vars['paged'], get_permalink( get_option( 'page_for_posts' ) ) );

Timber::render( 'views/templates/index.twig', $context );
