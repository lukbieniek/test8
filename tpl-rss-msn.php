<?php
/*
Template Name: RSS - MSN
*/
add_filter( 'embed_oembed_html', 'wpse_embed_oembed_html', 99, 4 );
function wpse_embed_oembed_html( $cache, $url, $attr, $post_ID ) {
    if ( false !== strpos( $url, 'facebook.com' ) ) {
        $ret = '<iframe frameborder="0" allowTransparency="true" src="'. $url .'"></iframe>';
    } elseif ( false !== strpos( $url, 'twitter.com' ) ) {        
        $cache = str_replace( '<blockquote class="twitter-tweet" data-width="500" data-dnt="true">', '<blockquote class="twitter-tweet" align="center" width="350">', $cache );
        $ret = $cache;
    } elseif ( false !== strpos( $url, 'spidersweb.pl/bizblog') ) {
        $cache = '';
        $ret = $cache;
    }  else {
        $ret = $cache;
    }

    return $ret;
}

function yoast_rss_date($timestamp = null)
{
    $timestamp = ($timestamp == null) ? time() : $timestamp;
    echo date(DATE_RSS, $timestamp);
}

function yoast_rss_text_limit($string, $length, $replacer = '...')
{
    $string = strip_tags($string);
    $string = explode('. ', $string);

    $ret = '';
    for ($i = 0; $i < $length; ++$i) {
        if ($i == 0) {
            $ret = $string[$i].'. ';
        } else {
            $ret = $ret.''.$string[$i].'. ';
        }
    }

    return $ret;
}
$numposts = 40;
if( isset( $_GET['cat'] ) ) {
    $cat = $_GET['cat'];
}

$args2 = array(
  'posts_per_page' => $numposts,
);

if ($cat == '') {
    //$args2['category__not_in'] = array(4295,4301,4296,4299,4297,4300,4298,4304);
} else {
    $args2['category__in'] = array($cat);
}

$args = array('numberposts' => 5);
//print_r($args2);
$myposts = get_posts($args2);

$i = 0;
$lastpost = $numposts - 1;

header('Content-Type: application/rss+xml; charset=UTF-8');
echo '<?xml version="1.0" encoding="UTF-8"?>';
?><rss version="2.0" xmlns:dc= "http://purl.org/dc/terms" xmlns:media= "http://search.yahoo.com/mrss/">
<channel>
  <title><?php echo get_bloginfo( 'name' ); ?></title>
  <link><?php echo get_bloginfo( 'url' ); ?></link>
  <description><?php echo get_bloginfo( 'description' ); ?></description>
  <language>pl</language>
  <pubDate><?php yoast_rss_date(time()); ?></pubDate>
  <lastBuildDate><?php yoast_rss_date(time()); ?></lastBuildDate>
  <managingEditor>press@whatnext.pl</managingEditor>
<?php foreach ($myposts as $post) :  setup_postdata($post); ?>

  <item>
    <id><?php echo $post->ID;?></id>
    <title><?php echo get_the_title($post->ID); ?></title>
    <link><?php echo get_permalink($post->ID); ?></link>
    <pubdate><?php yoast_rss_date(strtotime($post->post_date_gmt)); ?></pubdate>
    <author><?php the_author_meta('display_name'); ?></author>
    <media:content url="<?php $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID) , 'large'); echo $thumb[0];?>">
      <media:title>
        <?php $thumb_id = get_post_thumbnail_id($post->ID);
              $alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
              echo $alt;
        ?>
      </media:title>
      <media:credit>Provided by <?php echo get_bloginfo( 'name' ); ?></media:credit>
    </media:content>
    <description><![CDATA[
    <?php 
    $content = strip_tags( wpautop( apply_filters( 'the_content', strip_shortcodes( $post->post_content ) ) ), '<p><a><img><strong><em><h1><h2><h3><h4><iframe><blockquote>' );
    $content = str_replace( '<iframe width="500" height="375"', '<iframe width="100%" height="100%" style="display: block; margin-left: auto; margin-right: auto;"', $content  );
    $content = str_replace( '<iframe width="500" height="281"', '<iframe width="100%" height="100%" style="display: block; margin-left: auto; margin-right: auto;"', $content  );
    
    echo $content;
    ?>
    ]]></description>
  </item>
<?php ++$i; endforeach; ?>

</channel>
</rss>
