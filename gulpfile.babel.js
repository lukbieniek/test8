'use strict';

import plugins       from 'gulp-load-plugins';
import yargs         from 'yargs';
import browser       from 'browser-sync';
import gulp          from 'gulp';
import rimraf        from 'rimraf';
import yaml          from 'js-yaml';
import fs            from 'fs';
import webpackStream from 'webpack-stream';
import webpack2      from 'webpack';
import named         from 'vinyl-named';
import lazypipe      from 'lazypipe';
import Parker        from 'parker/lib/Parker';
import prettyJSON    from 'prettyjson';
import autoprefixer  from 'autoprefixer';
import sassLint      from 'gulp-sass-lint';
import eslint        from 'gulp-eslint';
import sassGlob      from 'gulp-sass-glob';
import shell         from 'gulp-shell';


// Load all Gulp plugins into one variable
const $ = plugins();

// Check for --production flag
const PRODUCTION = !!(yargs.argv.production);

// Load settings from settings.yml
const { COMPATIBILITY, PORT, UNCSS_OPTIONS, PATHS, DEVURL } = loadConfig();

//path to manifest
var revManifest = PATHS.dist + 'assets.json';

function loadConfig() {
  let ymlFile = fs.readFileSync('config.yml', 'utf8');
  return yaml.load(ymlFile);
}

// Build the "dist" folder by running all of the below tasks
gulp.task('build',
 gulp.series(clean, gulp.parallel(images, sass, javascript, copy), revision_css, revision_js ));

// Build the site, run the server, and watch for file changes
gulp.task('default',
  gulp.series('build', server, watch));

gulp.task('audit', gulp.series('build', function(done) {
  fs.readFile(PATHS.cssfile, function(err, data) {
    var parker = new Parker(require('parker/metrics/All'));
    var results = parker.run(data.toString());
    console.log(prettyJSON.render(results));
    done();
  });
}));

// SASS linting - configuration file .sass-lint-yml
gulp.task('sassLint', function() {
  return gulp.src('assets/scss/**/*.scss')
  .pipe(sassLint({
    config: './.sass-lint.yml'
  }))
  .pipe(sassLint.format())
  .pipe(sassLint.failOnError());
});

// JS linting - configuration file .eslintrc
gulp.task('esLint', function() {
  return gulp.src('assets/js/**/*.js')
  .pipe(eslint({
    useEslintrc: true,
    configFile: '.eslintrc'
  }))
  .pipe(eslint.format())
  .pipe(eslint.failAfterError());
});

// Delete the "dist" folder
// This happens every time a build starts
function clean(done) {
  rimraf(PATHS.dist, done);
}

function revision_css() {
	return gulp.src([ PATHS.dist + '/css/*.css' ], {base: PATHS.dist + '/css' })
  .pipe($.if(PRODUCTION, gulp.dest(PATHS.dist + '/css' )))
  .pipe($.if(PRODUCTION, $.rev() ))
  .pipe($.if(PRODUCTION, gulp.dest(PATHS.dist + '/css')))
  .pipe($.if(PRODUCTION, $.rev.manifest( PATHS.dist +'/manifest.json', {
    base: PATHS.dist,
    merge: true
  } )))
  .pipe($.if(PRODUCTION, gulp.dest(PATHS.dist)));
}

function revision_js() {
	return gulp.src([ PATHS.dist + '/js/*.js' ], {base: PATHS.dist + '/js' })
  .pipe($.if(PRODUCTION, gulp.dest(PATHS.dist + '/js' )))
  .pipe($.if(PRODUCTION, $.rev() ))
  .pipe($.if(PRODUCTION, gulp.dest(PATHS.dist + '/js')))
  .pipe($.if(PRODUCTION, $.rev.manifest( PATHS.dist +'/manifest.json', {
    base: PATHS.dist,
    merge: true
  } )))
  .pipe($.if(PRODUCTION, gulp.dest(PATHS.dist)));
}

// Copy files out of the assets folder
// This task skips over the "images", "js", and "scss" folders, which are parsed separately
function copy() {
  return gulp.src(PATHS.assets)
    .pipe(gulp.dest(PATHS.dist + '/'));
}

// Compile Sass into CSS
// In production, the CSS is compressed
function sass() {
  return gulp.src('assets/scss/**/*.scss')
    .pipe($.sourcemaps.init())
    .pipe($.plumber())
    .pipe(sassGlob())
    .pipe($.sass({
      includePaths: PATHS.sass,
      outputStyle: 'compressed'
    }).on('error', $.sass.logError))
      .on('error', () => {
        if (PRODUCTION){ throw new Error('Errors in SASS build.') }
    })
    .pipe($.postcss([autoprefixer()])) // uses ".browserslistrc"
    .pipe($.if(!PRODUCTION, $.sourcemaps.write()))
    .pipe(gulp.dest(PATHS.dist + '/css'))
    .pipe(browser.reload({ stream: true }));
}

let webpackConfig = {
  rules: [
    {
      test: /.js$/,
      use: [
        {
          loader: 'babel-loader'
        }
      ]
    }
  ]
}

// let webpackConfig = {
//   module: {
//     rules: [
//       {
//         test: /.js$/,
//         use: [
//           {
//             loader: 'babel-loader'
//           }
//         ]
//       }
//     ]
//   },
//   plugins: [
//     new webpack2.DefinePlugin({
//       'process.env.NODE_ENV': JSON.stringify('production')
//     }),
//     new webpack2.optimize.UglifyJsPlugin()
//   ]
// }

// Combine JavaScript into one file
// In production, the file is minified
function javascript() {
  return gulp.src(PATHS.entries)
    .pipe(named())
    .pipe($.sourcemaps.init())
    .pipe(webpackStream({module: webpackConfig}, webpack2))
    .pipe($.if(PRODUCTION, $.uglify()
      .on('error', e => { console.log(e); })
    ))
    .pipe($.if(!PRODUCTION, $.sourcemaps.write()))
    .pipe(gulp.dest(PATHS.dist + '/js'));

}

// Copy images to the "dist" folder
// In production, the images are compressed
function images() {
  return gulp.src('assets/images/**/*')
    .pipe($.if(PRODUCTION, $.imagemin({
      progressive: true
    })))
    .pipe(gulp.dest(PATHS.dist + '/images'));
}

// Start a server with BrowserSync to preview the site in
function server(done) {
  browser.init({
    files: ['{lib,templates}/**/*.php', '*.php'],
    proxy: DEVURL,
    snippetOptions: {
      whitelist: ['/wp-admin/admin-ajax.php'],
      blacklist: ['/wp-admin/**']
    }
  });
  done();
}

// Reload the browser with BrowserSync
function reload(done) {
  browser.reload();
  done();
}

// Watch for changes to static assets, pages, Sass, and JavaScript
function watch() {
  gulp.watch(PATHS.assets, copy);
  gulp.watch('assets/scss/**/*.scss').on('all', sass);
  gulp.watch('blocks/**/*.scss').on('all', sass);
  gulp.watch('assets/js/**/*.js').on('all', gulp.series(javascript, browser.reload));
  gulp.watch('assets/images/**/*').on('all', gulp.series(images, browser.reload));
  gulp.watch('**/*.php').on('all', gulp.series(browser.reload));
  gulp.watch('**/*.twig').on('all', gulp.series(browser.reload));

}


// fetch command line arguments
const arg = (argList => {
  let arg = {}, a, opt, thisOpt, curOpt;
  for (a = 0; a < argList.length; a++) {
    thisOpt = argList[a].trim();
    opt = thisOpt.replace(/^\-+/, '');
    if (opt === thisOpt) {
      // argument value
      if (curOpt) arg[curOpt] = opt;
      curOpt = null;
    }
    else {
      // argument name
      curOpt = opt;
      arg[curOpt] = true;
    }
  }
  return arg;
})(process.argv);

gulp.task('create_nroom_module', () => {
  const basicFile = [
    'nroom_modules/'+arg.dir,
    'nroom_modules/'+arg.dir+'/acf-fields',
    'nroom_modules/'+arg.dir+'/index.js',
    'nroom_modules/'+arg.dir+'/'+arg.dir+'.scss',
    'nroom_modules/'+arg.dir+'/functions',
    'nroom_modules/'+arg.dir+'/'+arg.dir+'.php'
  ];

  const nodeModuleFile = [
    'nroom_modules/'+arg.dir+'/package.json',
    'nroom_modules/'+arg.dir+'/.gitignore'
  ];

  const mainFileContnet = '<?php\n\
  add_filter("acf/settings/load_json", "sw_'+arg.dir+'_acf_json_load_point");\n\
  function sw_'+arg.dir+'_acf_json_load_point( $paths ) {\n\
    $paths[] = dirname(__FILE__) . "/acf-fields";\n\
    return $paths;\n\
  }\n\
  \n\
  foreach (glob( __DIR__ . "/functions/*.php") as $filename) {\n\
    include $filename;\n\
  }\n\
  ';

  if (arg.node_module) {
    var fileStructure = basicFile.concat(nodeModuleFile);
  } else {
    var fileStructure = basicFile;
  }

  const Bitbucket = require('bitbucket')
  const bitbucket = new Bitbucket()

  function createPackagejson(dir, arg, callback) {
    fs.writeFileSync(dir, '{\n\t"name": "'+arg.dir+'",\n\t"version": "0.0.1",\n\t"description": ""\n}');
    callback();
  }

  function createFileStructure(files, callback) {
    files.forEach(dir => {
      if (!fs.existsSync(dir)) {
        if (dir.indexOf('.') !== -1) {
          if (dir.indexOf('.php') !== -1) {
            fs.writeFileSync(dir, mainFileContnet);
          } else if (dir.indexOf('.gitignore') !== -1) {
            fs.writeFileSync(dir, '.DS_Store\nnode_modules');
          } else if (dir.indexOf('package.json') !== -1) {
            createPackagejson(dir, arg, function() {
              return gulp.src('*.js', {read: false})
              .pipe(shell([
                'npm install --save ./nroom_modules/'+arg.dir
              ]));
            });
          }else {
            fs.writeFileSync(dir, '');
          }
        } else {
          fs.mkdirSync(dir);
        }
        console.log('📁  folder created:', dir);
      } else {
        console.log('module exist');  
      }
    });
    callback();
  }

  function createGitSubmodules(repoFullName, callback) {
    return gulp.src('*.js', {read: false})
    .pipe(shell([
      'cd ../../../..',
      'git submodule add git@bitbucket.org:'+repoFullName+'.git web/app/themes/nroom/nroom_modules/'+arg.dir
    ]));
    callback();
  }

  function createModules(repoFullName) {
    createFileStructure(fileStructure, function () {
      return gulp.src('*.js', {read: false})
      .pipe(shell([
        'cd nroom_modules/'+arg.dir,
        'git init',
        'git add --all',
        'git commit -m "Initial Commit"',
        'git remote set-url origin git@bitbucket.org:'+repoFullName+'.git',
        'git push -u origin master'
      ]));
    });
  }

  if (!fs.existsSync(fileStructure[0])) {
    bitbucket.authenticate({
      type: 'basic',
      username: 'lukbieniek',
      password: 'kVYvnG8gYcjZSrnzJE9S'
    })
    bitbucket.repositories
      .create({ username: 'lukbieniek', repo_slug: arg.dir })
      .then(({ data, headers }) => createModules(data.full_name))
      .catch(err => console.error(err))
  }
});
