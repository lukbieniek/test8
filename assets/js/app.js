import $ from 'jquery';
import 'what-input';
import './lib/foundation-explicit-pieces.js';
import Router from './lib/router';

// Routes
import common from './routes/common';
import home from './routes/home';
import single from './routes/single';


window.$ = $;

/**
 * Populate Router instance with DOM routes
 * @type {Router} routes - An instance of our router
 */
const routes = new Router({
  common,
  home,
  single
});

/** Load Events */
$(document).ready(() => routes.loadEvents());
