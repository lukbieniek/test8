import $ from 'jquery';
import swLqip from 'sw-lqip';
import SimpleBar from 'simplebar';
import jQueryBridget from 'jquery-bridget';
import isotope from 'isotope-layout';
import Plyr from 'plyr';
import BeforeAfter from 'before-after';
import Slick from 'slick-carousel';
import loadNextPosts from "twig-to-react";



// footer accordion menu
function accordion(width) {
  if (width <= 640 ) {
    $(".footer__menu__title--ac").addClass("accordion");
    $(".footer__menu__list").addClass("disable");
    console.log(width);
  } else {
    $(".footer__menu__title--ac").removeClass("accordion");
    $(".footer__menu__list").removeClass("disable");
  }
  if (width <= 640 ) {
    $('.footer__menu__title').each(function () {
      var $this = $(this);
      $this.css('cursor','pointer');
      $this.on("click", function () {
        event.preventDefault()
        var menuId = $(this).data('menu-id');
        if (menuId == 2 ) {
          $(".footer__menu__list_2").toggleClass("disable");
          $(".footer__menu__list_3").toggleClass("disable");
        } else {
          $(".footer__menu__list_"+menuId).toggleClass("disable");
        }
      });
    });
  }

}
// Content padding
function contentPadding() {
  let padding = $(".row").offset().left;
  $(".js-p-l").css("margin-left", padding+15);
  $(".js-p-r").css("padding-right", padding+15);
  $(".special__post:last-child").css("margin-right", padding+15);
}

export default {
  init() {
    // JavaScript to be fired on all pages
    $(document).foundation();

    // Slide menu
    let navButton = document.querySelector(".nav-button");
    navButton.addEventListener("click", e => {
      e.preventDefault();
      // toggle nav state
      document.body.classList.toggle("nav-visible");
    });

    
    $(window).on("resize", function () {
      //accordion($(window).width());
      contentPadding();
    }).resize();
    
    $(".switch-info").hover(function(){
      $(".switch-info-box").toggleClass("active");
    });

    // Search form
    $('.header__search').on("click", function () {
      $(".header__search-form").addClass("active");
    });
    $('.searchform__close').on("click", function () {
      $(".header__search-form").removeClass("active");
    });
    var specifiedElement = document.getElementById('header__menu');
    document.addEventListener('click', function(event) {
        var isClickInside = specifiedElement.contains(event.target);
        if (isClickInside) {
          //console.log('You clicked inside')
        }
        else {
          //console.log('You clicked outsinside')
          $(".header__search-form").removeClass("active");
        }
    });

    // Dark mode
    jQuery("#dark-mode").click(function() {
      localStorage.setItem('mode', (localStorage.getItem('mode') || 'light') === 'dark' ? 'light' : 'dark');
      localStorage.getItem('mode') === 'dark' ? document.querySelector('body').classList.add('theme--dark') : document.querySelector('body').classList.remove('theme--dark');
      localStorage.getItem('mode') === 'light' ? document.querySelector('body').classList.add('theme--default') : document.querySelector('body').classList.remove('theme--default');
    })
    jQuery("#dark-mode-mobile").click(function() {
      localStorage.setItem('mode', (localStorage.getItem('mode') || 'light') === 'dark' ? 'light' : 'dark'); 
      localStorage.getItem('mode') === 'dark' ? document.querySelector('body').classList.add('theme--dark') : document.querySelector('body').classList.remove('theme--dark');
      localStorage.getItem('mode') === 'light' ? document.querySelector('body').classList.add('theme--default') : document.querySelector('body').classList.remove('theme--default');
    })
    if (localStorage.getItem('mode') === 'light' || !localStorage.getItem('mode')) {
      jQuery('body').addClass('theme--default');
      jQuery( "#dark-mode" ).prop( "false", true );
      jQuery( "#dark-mode-mobile" ).prop( "false", true );
    } else if (localStorage.getItem('mode') === 'dark') {
      jQuery('body').addClass('theme--dark');
      jQuery( "#dark-mode" ).prop( "checked", true );
      jQuery( "#dark-mode-mobile" ).prop( "checked", true );
    }


    // Isotpe block
    var Isotope = require('isotope-layout');
    jQueryBridget( 'isotope', Isotope, $ );
    function enableIsotope() {
      // for each container
      $('.promo-post').each( function( i, gridContainer ) {
        var $gridContainer = $( gridContainer );
        // init isotope for container
        var $grid = $gridContainer.find('.grid').isotope({
          itemSelector: '.element-item',
          layoutMode: 'masonry'
        });
        // initi filters for container
        $gridContainer.find('.filters-button-group').on( 'click', 'button', function() {
          var filterValue = $( this ).attr('data-filter');
          $grid.isotope({ filter: filterValue });
        });
      });
        
      $('.button-group').each( function( i, buttonGroup ) {
        var $buttonGroup = $( buttonGroup );
        $buttonGroup.on( 'click', 'button', function() {
          $buttonGroup.find('.is-checked').removeClass('is-checked');
          $( this ).addClass('is-checked');
        });
      });
    
    };
    enableIsotope();
    
    //const el = new SimpleBar(document.getElementById('special__posts'), { autoHide: false });
    //const el2 = new SimpleBar(document.getElementById('big-post'), { autoHide: false });

    //TODO połączyc to powtórzenie w jedną funkcję 
    $('.special').before().click(function () {
      var actualScroll = $(this).find(".simplebar-content").scrollLeft();
      $(this).find(".simplebar-content").animate({ scrollLeft: actualScroll+850 }, "fast");
    });

    $('.gallery').before().click(function () {
      var actualScroll = $(this).find(".simplebar-content").scrollLeft();
      $(this).find(".simplebar-content").animate({ scrollLeft: actualScroll+850 }, "fast");
    });

    const players = Array.from(document.querySelectorAll('.js-player')).map(p => new Plyr(p, {
      hideControls: false, 
      captions: {active: true}
    }));
    players.map(p => {
      p.toggleControls(false);
      p.on('play', event => {
        p.toggleControls(true);
      });
    });

    $( ".post-contents__open" ).click(function() {
      $( ".post-contents__list" ).fadeToggle( "fast", "linear" );
      $( ".post-contents__title span" ).toggleClass('active');
      $( ".post-contents__title .post-contents__btn" ).toggleClass('hide');
      $(this).fadeOut(function () {
        $(this).text(($(this).text() == 'Pokaż cały spis treści') ? 'zwiń spis treści' : 'Pokaż cały spis treści').fadeIn();
      })
    });

    var url = window.location.pathname, 
    urlRegExp = new RegExp(url.replace(/\/$/,'') + "$"); // create regexp to match current url pathname and remove trailing slash if present as it could collide with the link in navigation in case trailing slash wasn't present there
    // now grab every link from the navigation
    $('.post-contents__list a').each(function(){
        // and test its normalized href against the url pathname regexp
        if(urlRegExp.test(this.href.replace(/\/$/,''))){
            $(this).addClass('active');
        }
    });

    $(".slider").slick({
      infinite: false,
      slidesToShow: 3,
      slidesToScroll: 1,
      variableWidth: true,
      dots: true,
      useTransform: false,
      responsive: [{
        breakpoint: 640,
        settings: "unslick" // destroys slick
      }]
    });

    $(".swipe-gallery ul").slick({
      infinite: false,
      slidesToShow: 4,
      slidesToScroll: 1,
      variableWidth: true,
    });

    $('.in-preparation a').removeAttr('href');

    $( "#show-coments" ).click(function() {
      $("#discussion").toggleClass("active");
      $(this).css('display', 'none')
    });

    //TODO przeniesc do nroom_modules/share 
    function share_popup(url, winWidth, winHeight) {
      var winTop = (screen.height / 2) - (winHeight / 2);
      var winLeft = (screen.width / 2) - (winWidth / 2);
      window.open(url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,location=0,menubar=0,width=' + winWidth + ',height=' + winHeight);
    }
    jQuery( '.socialrev' ).on( 'click', function() {
      var modal_url = jQuery(this).attr( 'href' );
      share_popup( modal_url, 500, 500 );
      return false;
    });

    //TODO przeniesc do nroom_modules/twig-to-react 
    $('.showDisqus').on('click', function(){   // click event of the show comments button
      var this_ = $(this);
      var disqus_shortname = 'whatnextpl';
      var title = $(this).attr('data-title');
      var identifier = $(this).attr('data-id');
      var url = $(this).attr('data-url');
  
      if (window.DISQUS) {
  
          DISQUS.reset({ // Remove the old call
            reload: false,
            config: function () {
            this.page.identifier = window.old_identifier;
            this.page.url = window.old_url;
            this.page.title = window.old_title;
            }
          });
          $('.showDisqus').show();
          $('#disqus_thread').remove();
  
          $('<div id="disqus_thread"></div>').insertAfter(this_);
  
          setTimeout( function() { // Creates a new call DISQUS, with the new ID
              DISQUS.reset({
                reload: true,
                config: function () {
                this.page.identifier = identifier;
                this.page.url = url;
                this.page.title = title;
                }
              });
              window.old_identifier = identifier;
              window.old_url = url;
              window.old_title = title;
          });
  
      } else {
  
          var disqus_identifier = parseFloat(identifier);
          var disqus_title = title;
          var disqus_url = url;
  
          $('<div id="disqus_thread"></div>').insertAfter(this_);
  
          (function() {
              var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
              dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
              (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
          })();
  
          setTimeout( function() { // Sorry, there must be a better way to force the ID called correctly
              DISQUS.reset({
                reload: true,
                config: function () {
                this.page.identifier = identifier;
                this.page.url = url;
                this.page.title = title;
                }
              });
          },500);
  
          window.old_identifier = identifier;
          window.old_url = url;
          window.old_title = title;
  
      }
      $(this).fadeOut();  // remove the show comments button
    });

    $(".menu-item").hover(function() {
      var categorID = $(this).attr("data-catid");
      $(".mega-menu[data-catid='" + categorID + "']").toggleClass("active");
    });

    $(".mega-menu").hover(function() {
      $(this).toggleClass("active");
    });

    var postHeaderHeight = $(".post__header__content").outerHeight();
    var postHeaderHeightAfter = postHeaderHeight+88;
    $('head').append('<style>.container::after{height:'+postHeaderHeightAfter+'px;}</style>');

    loadNextPosts();

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
