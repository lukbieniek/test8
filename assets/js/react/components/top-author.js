import React from "react";


const topauthor = (props) => {

const { user_avatar ,user } = props;



return(

<div className="row large-uncollapse medium-uncollapse small-uncollapse align-middle">
  <div className="large-3 small-12 medium-12 columns">
    <div className="author-avatar">
      <img src={ user_avatar } alt="">
    </div>
  </div>
  <div className="large-9 small-12 medium-12 columns">
    <div className="top-archive top-archive--author">
      <div className="post-boks__tag">Redaktor</div>
      <div className="home__title">{ user.name }</div>
      <p>{ user.description }</p>
    </div>
  </div>
</div>
)

}

export default topauthor;