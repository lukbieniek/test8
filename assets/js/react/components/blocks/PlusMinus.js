import React from "react";


const PlusMinus = (props) => {

const { block,fields,item } = props;



return(

<div id={ block.id } className="plus-minus" data-easygut-id="plusminus">
  <div className="plus-minus__title">
    <div className="plus-minus__rating">
      <div className="plus-minus__rating__left">
        <div>ocena:</div>
        <div className="plus-minus__rating__num">{ fields.rate }</div>
      </div>
      <div className="plus-minus__rating__right">
        <div className="plus-minus__product">{ fields.name }</div>

        <Choose><When condition={ fields.price }>

          <div className="plus-minus__price">Cena: <span>{ fields.price }</span> zł</div>

        </When></Choose>

      </div>
    </div>

    <Choose><When condition={ fields.partner }>

      <div className="plus-minus__cta">
        <div><a href={ fields.partner_link } className="btn btn--e-commerce" href={ fields.url }>Gdzie kupić?</a></div>
        <div className="plus-minus__cta__partner">
          <Choose><When condition={ fields.partner_logo }>
            <img src={ fields.partner_logo } alt="partner logo">
          </When></Choose>
        </div>
      </div>

    </When></Choose>

  </div>
  <div className="plus-minus__content">

    <Choose><When condition={ fields.plusy }>
    <div className="plus-minus__left">
      <div className="plus-minus__content__title">Plusy</div>
      <div className="plus-minus__content__subtitle">Dlaczego warto?</div>
      <ul>
        {fields.plusy.map((item, index ) => (
          <li><span>{ item.plus }</span></li>
        ))}
      </ul>
    </div>
    </When></Choose>

    <Choose><When condition={ fields.minusy }>
    <div className="plus-minus__right">
      <div className="plus-minus__content__title">Minusy</div>
      <div className="plus-minus__content__subtitle">LEPIEJ ODPUŚCIĆ</div>
      <ul>
        {fields.minusy.map((item, index ) => (
          <li><span>{ item.minus }</span></li>
        ))}
      </ul>
    </div>  
    </When></Choose>
    
  </div>
</div>
)

}

export default PlusMinus;