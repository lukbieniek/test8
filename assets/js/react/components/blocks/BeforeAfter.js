import React from "react";


const BeforeAfter = (props) => {

const { block,fields } = props;



return(

<div id={ block.id } className="before-after" data-easygut-id="beforeafter">
  <div className="cocoen">
    <img src={ fields.image_1 } alt="">
    <img src={ fields.image_2 } alt="">
  </div>
  <figcaption>{ fields.description }</figcaption>
</div>
)

}

export default BeforeAfter;