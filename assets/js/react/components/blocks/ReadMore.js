import React from "react";
import PostBoks from "../PostBoks"

const ReadMore = (props) => {

const { block,fields,posts,post,not post } = props;



return(

<div id={ block.id } className="read-more" data-easygut-id="readmore">
  <div className="read-more__heading">
    <div className="read-more__title">{ fields.title }</div>

    <Choose><When condition={ fields.partenr }>

      <div className="special__brand">
        <div className="special__brand__txt">Partnerem cyklu jest:</div> 
        <div><img src={ fields.partenr_img } alt="logo"></div>
      </div>
      
    </When></Choose> 

  </div>
  <div className="medium-post read-more__slider" data-equalizer>
    {posts.map((post, index ) => (
      <div className="read-more__slide" data-equalizer-watch><PostBoks post={post} not post={not post}/></div>
    ))}
  </div>
</div>
)

}

export default ReadMore;