import React from "react";


const FullBgBoks = (props) => {

const { post } = props;



return(

<div className="post-boks post-boks-fullbg lazyload" style={{ backgroundImage:`url(${ post.nrm_image_src.oryginal })` }}>
  { post.nrm_hot_title } 
  <div className="post-boks__tag">{ post.nrm_top_level_categories }</div>
  <div className="post-boks__title"><a href={ post.link }>{ post.nrm_title }</a></div>
  { post.nrm_product_placement }
  <a className="over-link" href={ post.link }></a>
</div>
)

}

export default FullBgBoks;