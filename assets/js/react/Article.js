import React from 'react';
import $ from 'jquery';

export default class Article extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      link: this.props.post.link
    };
  }

  render(){
    const { post, theme, sidebar_posts } = this.props;
    function createContent() { return {__html: post.content}; }
    function share_popup(url, winWidth, winHeight) {
      var winTop = (screen.height / 2) - (winHeight / 2);
      var winLeft = (screen.width / 2) - (winWidth / 2);
      window.open(url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,location=0,menubar=0,width=' + winWidth + ',height=' + winHeight);
    }
    const share = (e) => {
      e.preventDefault();
      $( '.socialrev' ).on( 'click', function() {
        var modal_url = $(this).attr( 'data-href' );
        share_popup( modal_url, 500, 500 );
        return false;
      });
    }
    const handleClick = () => {
      $('.showDisqus').on('click', function(){   // click event of the show comments button
        var this_ = $(this);
        var disqus_shortname = 'whatnextpl';
        var title = $(this).attr('data-title');
        var identifier = $(this).attr('data-id');
        var url = $(this).attr('data-url');
    
        if (window.DISQUS) {
    
            DISQUS.reset({ // Remove the old call
              reload: false,
              config: function () {
              this.page.identifier = window.old_identifier;
              this.page.url = window.old_url;
              this.page.title = window.old_title;
              }
            });
            $('.showDisqus').show();
            $('#disqus_thread').remove();
    
            $('<div id="disqus_thread"></div>').insertAfter(this_);
    
            setTimeout( function() { // Creates a new call DISQUS, with the new ID
                DISQUS.reset({
                  reload: true,
                  config: function () {
                  this.page.identifier = identifier;
                  this.page.url = url;
                  this.page.title = title;
                  }
                });
                window.old_identifier = identifier;
                window.old_url = url;
                window.old_title = title;
            });
    
        } else {
    
            var disqus_identifier = parseFloat(identifier);
            var disqus_title = title;
            var disqus_url = url;
    
            $('<div id="disqus_thread"></div>').insertAfter(this_);
    
            (function() {
                var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
            })();
    
            setTimeout( function() { // Sorry, there must be a better way to force the ID called correctly
                DISQUS.reset({
                  reload: true,
                  config: function () {
                  this.page.identifier = identifier;
                  this.page.url = url;
                  this.page.title = title;
                  }
                });
            },500);
    
            window.old_identifier = identifier;
            window.old_url = url;
            window.old_title = title;
    
        }
        $(this).fadeOut();  // remove the show comments button
      });
    }
    return(

      <div className="container container--load">
        <div className="post post--load">
          <div className="post__header">
            <div className="row medium-uncollapse small-uncollapse large-uncollapse">
              <div className="large-12 small-12 medium-12 column">

                
                  <div className="post__cat">{ post.top_level_categories }</div>
                
                
                <h1 className="post__title">{ post.title }</h1>
                <div className="post__infos">
                  <div className="post__info">
                    <div className="post__author">
                      <div className="post__author__img"><a href={ post.nrm_author.link }><img src={ post.nrm_author.avatar } alt="author"/></a></div>
                      <div className="post__author__txt">
                        <div className="post__author__name"><a href={ post.nrm_author.link }>{ post.nrm_author.name }</a></div>
                        <div className="post__date">{ post.nrm_date }</div>
                      </div>
                    </div>
                  </div>
                  <div className="post__labels">
                    { post.product_placement }
                    { post.hot_title }
                    { post.brend }       
                  </div>
                </div>
                

                
                  <div className="post__thumbnail lazyload" data-bg={ post.nrm_image_src.oryginal }></div>
                

              </div>
            </div>
          </div>
        </div>
        <div className="row medium-collapse small-collapse large-uncollapse">
          <div className="large-8 small-12 medium-12 columns">
            <div className="post__content">
              <div dangerouslySetInnerHTML={createContent()} />
            </div>
            <div className="post__footer">
              <div className="post__share">
                <a data-href={ post.nrm_share_links.facebook } onClick={share} className="btn btn--fb socialrev">Udostępnij na FB</a>
                <a data-href={ post.nrm_share_links.mesenger } onClick={share} className="btn btn--msg socialrev"><span>Wyślij przez Messenger</span></a>
                <a data-href={ post.nrm_share_links.twiter } onClick={share} className="btn btn--twiter socialrev"><img src={ `${theme.link}/dist/images/twitter.svg` } alt="twitter" /></a>
              </div>
              <div className="post__tags">
                <div className="post__tags__title">TAGI:</div>
              </div>

              <div className="showDisqus btn btn--big" onClick={handleClick} data-title={ post.title } data-id={ `${theme.link}/?p=${post.ID}` } data-url={ post.link }>Pokaż komentarze</div>

            </div>
          </div>
          <div className="post__sidebar large-4 small-12 medium-12 columns">
            
          </div>
        </div>
      </div>

    );
  }
}